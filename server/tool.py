import sqlite3


connection = sqlite3.connect('./haw-hackaton.db')
cursor = connection.cursor()

cursor.execute("CREATE TABLE rooms (room_id INTEGER PRIMARY KEY AUTOINCREMENT, building TEXT, room INTEGER, usage TEXT, accessibility INTEGER)");
cursor.execute("CREATE TABLE jobs (job_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, description TEXT, room_id INTEGER, priority INTEGER, status BOOLEAN)");

