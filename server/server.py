from bottle import Bottle, run, static_file
import restapi
from os import path
import os

server = Bottle()

@server.get('/')
def serveFeedbackIndex():
    return static_file('index.html', root='.')

@server.get('/feedback/index.html')
def serveFeedbackIndex():
    return static_file('index.html', root='../feedback-app')


@server.get('/feedback/<filepath:re:.*>')
def getFeedbackWebsite(filepath):
    return static_file(filepath, root='../feedback-app')

@server.get('/client')
def serveClientIndex():
    return static_file('index.html', root='../client-app')


@server.get('/client/<filepath:re:.*>')
def getClientWebsite(filepath):
    return static_file(filepath, root='../client-app')

server.mount('/api', restapi.app)

print(os.listdir('.'))
# run(app, host='localhost', port=8080)
run(server, host='10.15.52.204', port=80)