from bottle import app, template, route, request, response, run, static_file
import os
app = app()

@app.get('/')
def serveIndex():
    print(os.listdir('../feedback-app'))
    return static_file('index.html', root='../feedback-app')


@app.get('/<filepath:re:.*>')
def getWebsite(filepath):
    return static_file(filepath, root='../feedback-app')