from bottle import app, template, route, request, response, run, static_file
import os

app = app()

@app.get('/')
def serveIndex():
    print(os.listdir('../client-app'))
    return static_file('index.html', root='../client-app')


@app.get('/<filepath:re:.*>')
def getWebsite(filepath):
    print(os.listdir('..'))
    return static_file(filepath, root='../client-app')