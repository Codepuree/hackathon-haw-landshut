# -*- coding: utf-8 -*-
import sqlite3
from bottle import app, template, route, request, response, run
from json import dumps, loads


app = app()
connection = sqlite3.connect('./haw-hackaton.db')
cursor = connection.cursor()


def addNamesToSelect(data, names):
    retVal = []
    for row in data:
        r = {}
        for index in range(len(names)):
            name = names[index]
            r[name] = row[index]
        retVal.append(r)

    return retVal

def assembleQuery(tablename, columnnames, values):
    return 'INSERT INTO {} ({}) VALUES ({})'.format(tablename, ', '.join(columnnames), ', '.join(['"{}"'.format(value) for value in values]))

@app.get('/<tablename>')
def auftrage(tablename):
    response.content_type = 'application/json'
    try:
        cursor.execute('SELECT * FROM ' + tablename.capitalize())
        names = [description[0] for description in cursor.description]
        return dumps(addNamesToSelect(cursor.fetchall(), names))
    except Exception:
        return dumps({"error": {"message": "Some error occured in the SQL."}})

@app.get('/getRoomID')
def getRoomID():
    if ('building' in request.query and 'room' in request.query):
        cursor.execute('SELECT * FROM rooms WHERE building="{}" AND room={}'.format(request.query['building'], request.query['room']))
        names = [description[0] for description in cursor.description]
        return addNamesToSelect(cursor.fetchall(), names)
    else:
        return dict({ error: 'One of the query parameters is missing.' })

@app.put('/<tablename>')
def addAuftrag(tablename):
    response.content_type = 'application/json'
    columnnames = []
    values = []
    data = loads(next(iter(request.forms.dict)).replace('Ã¼', 'ü'))
    for columnname in data:
        columnnames.append(columnname)
        if (type(data[columnname]) is bool):
            if (data[columnname]):
                values.append(1)
            else:
                values.append(0)
        else:
            values.append(data[columnname])

    print('SQL:', assembleQuery(tablename, columnnames, values))
    cursor.execute(assembleQuery(tablename, columnnames, values))
    connection.commit()
    return assembleQuery(tablename, columnnames, values)
